#! /usr/bin/env node
"use strict";
const today = new Date(),
    age = today.getFullYear() - 1991;

if (today.getDate() == 31 && today.getMonth() == 4) {
    console.log("\n \n \n");
    console.log("˜˜”*°•.¸☆ ★ ☆¸.•°*”˜˜”*°•.¸☆");
    console.log("╔╗╔╦══╦═╦═╦╗╔╗ ★ ★ ★");
    console.log("║╚╝║══║═║═║╚╝║ ☆¸.•°*”˜˜”*°•.¸☆");
    console.log("║╔╗║╔╗║╔╣╔╩╗╔╝ ★ Happy Birthday to Matteo Toto <3 ☆");
    console.log("╝╚╩╝╚╩╝╚╝═╚╝ ♥￥☆★★☆￥♥ ★☆   " + age + "y");
    console.log("\n[Built with love and Node JS ♥]");
    console.log("[Remember not to forget JS ♥] \n \n ");
} else {
    console.log("\n \n \n ");
    console.log(" ¯\\_(ツ)_/¯ I'm sorry it's not Matteo's birthday today");
    console.log("return 31/06... \n \n \n ");
}